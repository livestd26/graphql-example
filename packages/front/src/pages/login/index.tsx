import React from 'react';
import { useHistory } from 'react-router-dom';
import { useQuery } from 'utils/routerUtils';
import Login from 'containers/Login';

const LoginPage = () => {
  const query = useQuery();
  const history = useHistory();

  const goToRoot = () => {
    const path = (query.params.b || '/') as string;
    history.push(path);
  };

  return <Login onSuccess={goToRoot} />;
};

export default LoginPage;
