import React from 'react';
import { shallow } from 'enzyme';

import Spinner from 'components/Spinner';

import Wrapper from './Wrapper';

describe('Wrapper component', () => {
  it('renders without crashing', () => {
    shallow(<Wrapper />);
  });

  it('renders loading without props', () => {
    const result = shallow(<Wrapper />);

    expect(
      result.matchesElement(
        <div>
          <Spinner />
        </div>,
      ),
    ).toBeTruthy();
  });

  it('renders error', () => {
    const result = shallow(<Wrapper error={true} />);

    expect(result.text()).toEqual('Some error occurred. Please try again later.');
  });

  it('renders custom error message', () => {
    const result = shallow(<Wrapper error={true} errorMessage={'customError'} />);

    expect(result.text()).toEqual('customError');
  });

  it('renders children', () => {
    const result = shallow(
      <Wrapper loading={false}>
        <Spinner />
      </Wrapper>,
    );

    expect(result.matchesElement(<Spinner />)).toBeTruthy();
  });

  it('catch error', () => {
    const result = shallow(
      <Wrapper loading={false}>
        <Spinner />
      </Wrapper>,
    );

    expect(result.matchesElement(<Spinner />)).toBeTruthy();

    const error = new Error('Children render error!');
    result.find(Spinner).simulateError(error);

    expect(result.find('div[data-testid="loadingError"]')).toHaveLength(1);
  });
});
