import React, { Component, ErrorInfo, PropsWithChildren } from 'react';
import Spinner from 'components/Spinner';

import s from './Wrapper.module.scss';

interface WrapperProps {
  loading?: boolean;
  error?: boolean;
  errorMessage?: string;
}
interface WrapperState {
  hasError: boolean;
}

export default class Wrapper extends Component<PropsWithChildren<WrapperProps>, WrapperState> {
  state: WrapperState = { hasError: false };

  static getDerivedStateFromError() {
    return { hasError: true };
  }

  componentDidCatch(error: Error, errorInfo: ErrorInfo) {
    // Можно также сохранить информацию об ошибке в соответствующую службу журнала ошибок
    console.log('Wrapper did catch', error.message);
  }

  render() {
    const { children, loading = true, error, errorMessage } = this.props;

    if (error || this.state.hasError) {
      return (
        <div className={s.loader} data-testid="loadingError">
          {errorMessage || 'Some error occurred. Please try again later.'}
        </div>
      );
    } else if (loading) {
      return (
        <div className={s.loader}>
          <Spinner />
        </div>
      );
    }

    return children;
  }
}
