import React, { useCallback } from 'react';
import cookie from 'cookie';

interface Props {
  onSuccess?: () => void;
}

const LoginOutButton = ({ onSuccess }: Props) => {
  const logout = useCallback(() => {
    document.cookie = cookie.serialize('isAuth', 'true', {
      maxAge: 0, // 1 hour
    });
    onSuccess && onSuccess();
  }, [onSuccess]);

  return <button onClick={logout}>Logout</button>;
};

export default LoginOutButton;
