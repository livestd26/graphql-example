import React from 'react';
import { shallow } from 'enzyme';
import Spinner from '../Spinner';

describe('Spinner component', () => {
  it('renders without crashing', () => {
    shallow(<Spinner />);
  });

  it('renders with custom className', () => {
    const result = shallow(<Spinner className="custom" />);

    expect(result.find('.custom')).toHaveLength(1);
  });
});
