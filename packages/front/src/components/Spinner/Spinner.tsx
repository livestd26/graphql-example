import React from 'react';
import cx from 'classnames';

import s from './Spinner.module.scss';

const Spinner = ({ className }: { className?: string }) => {
  return (
    <svg className={cx(s.spinner, className)} viewBox="0 0 50 50">
      <circle className={s.path} cx="25" cy="25" r="20" fill="none" strokeWidth="5" />
    </svg>
  );
};

export default Spinner;
