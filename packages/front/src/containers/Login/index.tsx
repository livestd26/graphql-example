import React, { useCallback } from 'react';
import cookie from 'cookie';

interface Props {
  onSuccess?: () => void;
}

const Login = ({ onSuccess }: Props) => {
  const login = useCallback(() => {
    document.cookie = cookie.serialize('isAuth', 'true', {
      maxAge: 60 * 60, // 1 hour
    });
    onSuccess && onSuccess();
  }, [onSuccess]);

  return <button onClick={login}>Login</button>;
};

export default Login;
