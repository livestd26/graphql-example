export const goToLogin = () => {
  const back = encodeURIComponent(
    window.location.pathname + (window.location.search ? window.location.search : ''),
  );
  window.location.replace(`/login?b=${back}`);
};
