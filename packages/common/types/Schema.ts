import { MergeInfo } from 'graphql-tools';
import { GraphQLResolveInfo } from 'graphql';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type ResolverFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo & { mergeInfo: MergeInfo }
) => Promise<TResult> | TResult;

export type RequireFields<T, K extends keyof T> = { [X in Exclude<keyof T, K>]?: T[X] } & { [P in K]-?: NonNullable<T[P]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string | number;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type Node = {
  id: Scalars['ID'];
  name: Scalars['String'];
};

export type Director = Node & {
  __typename?: 'Director';
  id: Scalars['ID'];
  movieIds: Array<Scalars['ID']>;
  movies?: Maybe<Array<Movie>>;
  name: Scalars['String'];
};

export type Movie = Node & {
  __typename?: 'Movie';
  director?: Maybe<Director>;
  directorId: Scalars['ID'];
  genre: Scalars['String'];
  id: Scalars['ID'];
  name: Scalars['String'];
};

export type Movies = {
  __typename?: 'Movies';
  movies: Array<Movie>;
};

export type Inner = {
  __typename?: 'Inner';
  movie?: Maybe<Movie>;
  movies?: Maybe<Array<Maybe<Movie>>>;
  director?: Maybe<Director>;
  entities?: Maybe<Array<Node>>;
};


export type InnerMovieArgs = {
  movieId: Scalars['ID'];
};


export type InnerMoviesArgs = {
  movieIds: Array<Scalars['ID']>;
};


export type InnerDirectorArgs = {
  directorId: Scalars['ID'];
};


export type InnerEntitiesArgs = {
  limit: Scalars['Int'];
};

export type Query = {
  __typename?: 'Query';
  inner?: Maybe<Inner>;
};



export type ResolverTypeWrapper<T> = Promise<T> | T;


export type LegacyStitchingResolver<TResult, TParent, TContext, TArgs> = {
  fragment: string;
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};

export type NewStitchingResolver<TResult, TParent, TContext, TArgs> = {
  selectionSet: string;
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};
export type StitchingResolver<TResult, TParent, TContext, TArgs> = LegacyStitchingResolver<TResult, TParent, TContext, TArgs> | NewStitchingResolver<TResult, TParent, TContext, TArgs>;
export type Resolver<TResult, TParent = {}, TContext = {}, TArgs = {}> =
  | ResolverFn<TResult, TParent, TContext, TArgs>
  | StitchingResolver<TResult, TParent, TContext, TArgs>;

export type SubscriptionSubscribeFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => AsyncIterator<TResult> | Promise<AsyncIterator<TResult>>;

export type SubscriptionResolveFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

export interface SubscriptionSubscriberObject<TResult, TKey extends string, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<{ [key in TKey]: TResult }, TParent, TContext, TArgs>;
  resolve?: SubscriptionResolveFn<TResult, { [key in TKey]: TResult }, TContext, TArgs>;
}

export interface SubscriptionResolverObject<TResult, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<any, TParent, TContext, TArgs>;
  resolve: SubscriptionResolveFn<TResult, any, TContext, TArgs>;
}

export type SubscriptionObject<TResult, TKey extends string, TParent, TContext, TArgs> =
  | SubscriptionSubscriberObject<TResult, TKey, TParent, TContext, TArgs>
  | SubscriptionResolverObject<TResult, TParent, TContext, TArgs>;

export type SubscriptionResolver<TResult, TKey extends string, TParent = {}, TContext = {}, TArgs = {}> =
  | ((...args: any[]) => SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>)
  | SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>;

export type TypeResolveFn<TTypes, TParent = {}, TContext = {}> = (
  parent: TParent,
  context: TContext,
  info: GraphQLResolveInfo
) => Maybe<TTypes> | Promise<Maybe<TTypes>>;

export type IsTypeOfResolverFn<T = {}> = (obj: T, info: GraphQLResolveInfo) => boolean | Promise<boolean>;

export type NextResolverFn<T> = () => Promise<T>;

export type DirectiveResolverFn<TResult = {}, TParent = {}, TContext = {}, TArgs = {}> = (
  next: NextResolverFn<TResult>,
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

/** Mapping between all available schema types and the resolvers types */
export type ResolversTypes = {
  Node: ResolversTypes['Director'] | ResolversTypes['Movie'];
  ID: ResolverTypeWrapper<Scalars['ID']>;
  String: ResolverTypeWrapper<Scalars['String']>;
  Director: ResolverTypeWrapper<Director>;
  Movie: ResolverTypeWrapper<Movie>;
  Movies: ResolverTypeWrapper<Movies>;
  Inner: ResolverTypeWrapper<Inner>;
  Int: ResolverTypeWrapper<Scalars['Int']>;
  Query: ResolverTypeWrapper<{}>;
  Boolean: ResolverTypeWrapper<Scalars['Boolean']>;
};

/** Mapping between all available schema types and the resolvers parents */
export type ResolversParentTypes = {
  Node: ResolversParentTypes['Director'] | ResolversParentTypes['Movie'];
  ID: Scalars['ID'];
  String: Scalars['String'];
  Director: Director;
  Movie: Movie;
  Movies: Movies;
  Inner: Inner;
  Int: Scalars['Int'];
  Query: {};
  Boolean: Scalars['Boolean'];
};

export type NodeResolvers<ContextType = any, ParentType extends ResolversParentTypes['Node'] = ResolversParentTypes['Node']> = {
  __resolveType: TypeResolveFn<'Director' | 'Movie', ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
};

export type DirectorResolvers<ContextType = any, ParentType extends ResolversParentTypes['Director'] = ResolversParentTypes['Director']> = {
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  movieIds?: Resolver<Array<ResolversTypes['ID']>, ParentType, ContextType>;
  movies?: Resolver<Maybe<Array<ResolversTypes['Movie']>>, ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType>;
};

export type MovieResolvers<ContextType = any, ParentType extends ResolversParentTypes['Movie'] = ResolversParentTypes['Movie']> = {
  director?: Resolver<Maybe<ResolversTypes['Director']>, ParentType, ContextType>;
  directorId?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  genre?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType>;
};

export type MoviesResolvers<ContextType = any, ParentType extends ResolversParentTypes['Movies'] = ResolversParentTypes['Movies']> = {
  movies?: Resolver<Array<ResolversTypes['Movie']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType>;
};

export type InnerResolvers<ContextType = any, ParentType extends ResolversParentTypes['Inner'] = ResolversParentTypes['Inner']> = {
  movie?: Resolver<Maybe<ResolversTypes['Movie']>, ParentType, ContextType, RequireFields<InnerMovieArgs, 'movieId'>>;
  movies?: Resolver<Maybe<Array<Maybe<ResolversTypes['Movie']>>>, ParentType, ContextType, RequireFields<InnerMoviesArgs, 'movieIds'>>;
  director?: Resolver<Maybe<ResolversTypes['Director']>, ParentType, ContextType, RequireFields<InnerDirectorArgs, 'directorId'>>;
  entities?: Resolver<Maybe<Array<ResolversTypes['Node']>>, ParentType, ContextType, RequireFields<InnerEntitiesArgs, 'limit'>>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType>;
};

export type QueryResolvers<ContextType = any, ParentType extends ResolversParentTypes['Query'] = ResolversParentTypes['Query']> = {
  inner?: Resolver<Maybe<ResolversTypes['Inner']>, ParentType, ContextType>;
};

export type Resolvers<ContextType = any> = {
  Node?: NodeResolvers<ContextType>;
  Director?: DirectorResolvers<ContextType>;
  Movie?: MovieResolvers<ContextType>;
  Movies?: MoviesResolvers<ContextType>;
  Inner?: InnerResolvers<ContextType>;
  Query?: QueryResolvers<ContextType>;
};


/**
 * @deprecated
 * Use "Resolvers" root object instead. If you wish to get "IResolvers", add "typesPrefix: I" to your config.
 */
export type IResolvers<ContextType = any> = Resolvers<ContextType>;
