import { importSchema } from 'graphql-import';
import { gql } from 'apollo-server-core';
import { join } from 'path';

export const mainSchema = gql(importSchema(join(__dirname, 'main.schema.graphql')));