import { IFieldResolver } from 'apollo-server';
import { Context } from 'types/Context';
import { Directors, Movies } from 'mock/Data';
import {
  Scalars,
  Movie,
  Resolvers,
  InnerResolvers,
} from 'common/types';

type MovieIds = { movieIds: Scalars['ID'][] };
type DirectorId = { directorId: Scalars['ID'] };

// field resolvers
const movies: IFieldResolver<MovieIds, Context> = async (source) => {
  return source.movieIds.map(id => Movies.find(m => m.id === +id));
};
const director: IFieldResolver<DirectorId, Context> = async (source) => {
  return Directors.find(m => m.id === +source.directorId);
};

// inner resolvers
const movieFn: InnerResolvers<Context>['movie'] = (source, args) => {
  return Movies.find(m => m.id === +args.movieId) || null;
};
const moviesFn: InnerResolvers<Context>['movies'] = (source, args, context, info) => {
  info.fieldNodes.map(n => console.log(n.selectionSet?.selections));
  return args.movieIds.reduce((r, id) => {
    const i = Movies.find(m => m.id === +id);
    return i ? [...r, i]: r
  }, <Movie[]>[]);
};
const directorFn: InnerResolvers<Context>['director'] = (source, args) => {
  return Directors.find(m => m.id === +args.directorId) || null;
};
const entitiesFn: InnerResolvers<Context>['entities'] = (source, args) => {
  return [...Directors, ...Movies].splice(0, args.limit || 5);
};

// query resolvers
const resolvers: Resolvers<Context> = {
  Query: {
    inner: async (source, args, context, info) => {
      if (context.customMethod(info)) {
        return {}
      }
      //if dev throw
      return null;
    },
  },
  Inner: {
    movies: moviesFn,
    movie: movieFn,
    director: directorFn,
    entities: entitiesFn
  },
  Node: {
    __resolveType(obj, context , info) {
      return 'directorId' in obj ? 'Movie': 'Director'
    }
  },
  Movie: {
    director: director
  },
  Director: {
    movies: movies
  }
};

export default resolvers;